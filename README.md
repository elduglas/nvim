# Configuración NeoVIM como VCode

 - Descargar la versión más reciente de NeoVIM https://github.com/neovim/neovim-releases/releases
 - sudo apt purge nvim*
 - sudo dpkg -i paqueteNvim.deb
 - Instalar vim plug https://github.com/junegunn/vim-plug
 - mkdir ~/.config/nvim
 - Copiar la configuración dentro de la carpeta ~/.config/nvim
 - nvim ~/.config/nvim/init.vim
 - Dentro del archivo ejecutar PlugInstall
 - Listo!!!

