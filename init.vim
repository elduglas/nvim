syntax on

set mouse=a
set noerrorbells
set sw=2
set expandtab
set smartindent
set number
set rnu
set numberwidth=1
set nowrap
set noswapfile
set nobackup
set incsearch
set ignorecase
set clipboard=unnamedplus
set encoding=utf-8
set cursorline
set termguicolors
set backspace=2
set guioptions-=T
set guioptions-=L

set showtabline=2

let mapleader = " "

call plug#begin()

"Themes
Plug 'joshdick/onedark.vim'
Plug 'tomasr/molokai'
Plug 'catppuccin/nvim', { 'as': 'catppuccin' }

"IDE
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'akinsho/nvim-bufferline.lua'
Plug 'ryanoasis/vim-devicons'
Plug 'lilydjwg/colorizer'
Plug 'Yggdroot/indentLine'
Plug 'windwp/nvim-autopairs'
Plug 'sudar/comments.vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-pack/nvim-spectre'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'wfxr/minimap.vim'

call plug#end()

"Confioguraciones Extras
"colorscheme onedark
"MOLOKAI THEME
let g:molokai_original = 1
let g:rehash256 = 1
"colorscheme molokai
colorscheme catppuccin-frappe

"Atajos de teclado
"Guardar archivo
nmap <C-s> :w <CR>

"Cerrar ventana actual
nmap <C-w> :q <CR>

"buscar texto en la ventana actual
nmap <C-f> /

"Recargar configuracion del vim
nmap <leader>so :so% <CR>

"Abrir arbol.
nmap <C-b> :NERDTreeToggle<CR>

"Abre la terminal
nmap <C-t> :term<CR>

"Split de la ventana vertical.
nnoremap <leader>sv :vsp<CR>

"Split de la ventana horizontal.
nnoremap <leader>sh :sp<CR>

"TAB en modo normal se movera al siguient e buffer
nnoremap <silent><TAB> :bnext<CR>

"SHIFT-TAB va al tab anterior
nnoremap <silent><S-TAB> :bprevious<CR>

"Cierra el buffer
nmap <leader>bd :bdelete<CR>

let NERDTreeShowHidden=1
let g:indentLine_setColors = 0

"Comentarios
let g:comments_map_keys = 0

" key-mappings for comment line in normal mode
noremap  <silent> <C-l> :call CommentLine()<CR>
" key-mappings for range comment lines in visual <Shift-V> mode
vnoremap <silent> <C-l> :call RangeCommentLine()<CR>

" key-mappings for un-comment line in normal mode
noremap  <silent> <C-k> :call UnCommentLine()<CR>
" key-mappings for range un-comment lines in visual <Shift-V> mode
vnoremap <silent> <C-k> :call RangeUnCommentLine()<CR>

"Copiar - Pegar
nnoremap <C-c> "+y
vnoremap <C-c> "+y
nnoremap <C-v> "+p
vnoremap <C-v> "+p

"Deshacer
nnoremap <C-z> :u<CR>

" Configuración para nvim-spectre
let g:spectre_show_line = 1
let g:spectre_ignore_case = 1
let g:spectre_new_file_icons = 1
let g:spectre_file_sorters = ['v:true', 'v:false']
let g:spectre_color_devicons = 1

" Mapeos de teclado para nvim-spectre
nnoremap <silent> <leader>ss :Spectre<CR>
vnoremap <silent> <leader>sd :Spectre<CR>
nnoremap <silent> <leader>sw :SpectreWrite<CR>
vnoremap <silent> <leader>sW :SpectreWrite<CR>

"Abre el buscador de archivos
nmap <C-p> :FZF<CR>

"Minimapa
let g:minimap_width = 10
let g:minimap_auto_start = 1
let g:minimap_auto_start_win_enter = 1

lua << END
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}

require("bufferline").setup{}

require("nvim-autopairs").setup {}

require('spectre').setup()

END
